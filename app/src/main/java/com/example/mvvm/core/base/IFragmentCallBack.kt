package com.example.mvvm.core.base

interface IFragmentCallBack {
    fun replaceFragment(fragment: BaseFragment)
    fun replaceFragmentWithBackStack(fragment: BaseFragment)
    fun replaceFragmentWithRoot()
    fun replaceInFullscreenFrame(fragment: BaseFragment, addToBackStack: Boolean = false)
}