package com.example.mvvm.core.base

import com.example.mvvm.data.remote.ApiResponse
import com.example.mvvm.data.remote.RetrofitResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext


open class BaseRepository {}
//    fun <T> execute(response: RetrofitResult<ApiResponse<T>>): APIResult<T> {
//        return if (response is RetrofitResult.RetrofitSuccess) {
//            if (response.data?.succeeded == true) {
//                APIResult.Success(response.data.result)
//            } else {
//                APIResult.Error(response.data?.error?.code ?: 0, Exception("ERROR"))
//            }
//        } else if (response is RetrofitResult.RetrofitFailure) {
//            APIResult.Error(response.statusCode, response.e)
//        } else {
//            APIResult.Error(0, Exception("خطای عمومی"))
//        }
//    }
//
//    suspend fun <T> executeAsync(block: () -> T): T = withContext(Dispatchers.IO) {
//        val task = async {
//            block()
//        }
//
//        try {
//            return@withContext task.await()
//        } catch (e: Exception) {
//            throw e
//        }
//    }
//}
//
//
//sealed class APIResult<out T> {
//    data class Success<T>(val data: T?) : APIResult<T>()
//    data class Error(val statusCode: Int?, val e: Exception) : APIResult<Nothing>()
//}