package com.example.mvvm.core.base

import androidx.lifecycle.ViewModel
import com.example.mvvm.core.mvvm.SingleLiveEvent


open class BaseViewModel : ViewModel() {

    var mError = SingleLiveEvent<String>()

//    protected fun checkResponseError(result: APIResult.Error) {
//        mError.value = result.e.message
//    }

}