package com.example.mvvm.core.extention

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.bumptech.glide.request.RequestOptions
import org.koin.core.KoinComponent
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.Headers
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.Request
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.*
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import org.koin.core.inject


@GlideModule
class AppGlideModule : AppGlideModule()

    object ImageLoaderHelper {

        private const val CARD_CURVE_SIZE = 16

        fun load(
            context: Context?,
            url: String?,
            imageView: ImageView,
            isCurved: Boolean = false,
            isCircle: Boolean = false,
            isAbsoluteUrl: Boolean = false
        ) {
            context?.let {
                val glideReq = GlideApp.with(context)
                    .load(if (isAbsoluteUrl) url else url?.toAbsoluteImageURL())
                    .transition(withCrossFade())

                if (isCurved) glideReq.apply(getOptionCurve())
                if (isCircle) glideReq.apply(RequestOptions.circleCropTransform())

                glideReq.into(imageView)
            }
        }

        private fun getOptionCurve() =
            RequestOptions().transform(RoundedCorners(CARD_CURVE_SIZE.toPx()))


    }
