package com.example.mvvm.core.di

import com.example.mvvm.BuildConfig
import com.example.mvvm.data.remote.ApiClient
import com.example.mvvm.data.repository.SearchApi
import com.example.mvvm.data.repository.SearchRepository
import com.example.mvvm.features.searchVideos.SearchVideosViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module


private val module = module {
    single {
        SearchRepository(ApiClient.getRetrofitInstance().create(SearchApi::class.java))
    }
    viewModel { SearchVideosViewModel(get()) }

}

val diModules = listOf(module)
