package com.example.mvvm.core

import com.example.mvvm.MyApplication
import com.example.mvvm.core.PreferenceHelper
import com.example.mvvm.core.PreferenceHelper.get
import com.example.mvvm.core.PreferenceHelper.set
import java.util.*

object SharePrefManager {
    private val prefs =
        PreferenceHelper.customPrefs(MyApplication.mContext, Const.SharedPreference.PrefName)

}

