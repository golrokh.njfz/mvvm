package com.example.mvvm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.mvvm.core.base.BaseActivity
import com.example.mvvm.core.base.BaseFragment
import com.example.mvvm.core.base.IFragmentCallBack
import com.example.mvvm.core.extention.replaceFragmentInActivity
import com.example.mvvm.features.searchVideos.SearchVideosFragment

class MainActivity : BaseActivity(),IFragmentCallBack {

    override fun getContentView(): Int? = R.layout.activity_main

    override fun initView() {
        replaceFragment(SearchVideosFragment.newInstance())
    }

    override fun clickListeners() {

    }

    override fun subscribeViews() {

    }

    override fun executeInitialTasks() {
    }

    override fun replaceFragment(fragment: BaseFragment) {

        replaceFragmentInActivity(
            fragment,
            R.id.container,
            supportFragmentManager,
            hasBackStack = false
        )
    }

    override fun replaceFragmentWithBackStack(fragment: BaseFragment) {
    }

    override fun replaceFragmentWithRoot() {
    }

    override fun replaceInFullscreenFrame(fragment: BaseFragment, addToBackStack: Boolean) {
    }
}