package com.example.mvvm.features.searchVideos

import androidx.lifecycle.viewModelScope
import com.example.mvvm.core.base.BaseViewModel
import com.example.mvvm.core.mvvm.SingleLiveEvent
import com.example.mvvm.data.repository.SearchRepository
import com.example.mvvm.data.repository.model.SearchDataModel
import com.example.mvvm.data.repository.model.SearchResultListRsp
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import androidx.lifecycle.liveData
import com.example.mvvm.core.di.Resource

class SearchVideosViewModel(private var searchRepository: SearchRepository) : BaseViewModel() {

    val episodeList = SingleLiveEvent<SearchResultListRsp>()

//    fun getSearchResult(query: String, page: Int) = viewModelScope.launch {
//        val result = searchRepository.getSearchResult(q = query, p = page)
//        if (result is APIResult.Success) {
//            episodeList.value = result.data
//        } else if (result is APIResult.Error) {
//            checkResponseError(result)
//        }
//    }

    fun getSearchResult(q:String) = liveData(Dispatchers.IO) {
        emit(Resource.loading(data = null))
        try {
            emit(Resource.success(data = searchRepository.getSearchResult(q,1)))
        } catch (exception: Exception) {
            emit(Resource.error(data = null, message = exception.message ?: "Error Occurred!"))
        }
    }
}