package com.example.mvvm.features.searchVideos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.mvvm.R
import com.example.mvvm.core.extention.ImageLoaderHelper
import com.example.mvvm.data.repository.model.SearchDataModel
import kotlinx.android.synthetic.main.search_item_row.view.*


class SearchResultAdapter(private val itemSelect: ((SearchDataModel) -> Unit)) :
    RecyclerView.Adapter<SearchResultAdapter.ViewHolder>() {


   private var mSearchList = mutableListOf<SearchDataModel>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.search_item_row, parent, false)
        view.setOnClickListener {
            it.tag?.let { tag ->
                if (tag is SearchDataModel) {
                    itemSelect(tag)
                }
            }
        }
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(mSearchList[position])
    }

    override fun getItemCount(): Int {
        return mSearchList.size
    }

    inner class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {


        fun bind(searchDataModel: SearchDataModel) {

            ImageLoaderHelper.load(
                view.context,
                searchDataModel.pictures?.sizes?.get(3)?.link,
                view.movieImg,
                isCircle = false,
                isCurved = true,
                isAbsoluteUrl = true
            )
            view.titleTxt.text = searchDataModel.name
            view.descriptionTxt.text = searchDataModel.description
            view.tag = searchDataModel
        }

    }

    fun addAll(searchList :List<SearchDataModel>){
        mSearchList.clear()
        mSearchList.addAll(searchList)
        notifyDataSetChanged()
    }
}