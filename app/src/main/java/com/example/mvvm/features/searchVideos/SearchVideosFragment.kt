package com.example.mvvm.features.searchVideos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvvm.R
import com.example.mvvm.core.base.BaseFragment
import com.example.mvvm.core.di.Status
import kotlinx.android.synthetic.main.fragment_search_videos.*
import org.koin.android.viewmodel.ext.android.viewModel


class SearchVideosFragment : BaseFragment() {

    private val searchVideosViewModel: SearchVideosViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun getResourceId(): Int? = R.layout.fragment_search_videos
    override fun initView() {
    }

    override fun clickListeners() {
        searchImg.setOnClickListener {
            if (!searchEdt.text.isNullOrEmpty()) {
             search(searchEdt.text.toString().trim())
            }
        }

    }

    override fun executeInitialTasks() {
    }


    override fun subscribeViews() {
    }


    override fun onBackPressed(): Boolean {
        return false
    }

   private fun search(q: String) {
        searchVideosViewModel.getSearchResult(q).observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        resource.data?.let { response -> response.videos?.let { videos ->
                            var searchResultAdapter = SearchResultAdapter() {
                                // go to detail
                            }
                            videos?.let { it1 ->
                                it1?.let { it2 ->
                                    searchResultAdapter.addAll(
                                        it2
                                    )
                                }
                            }
                            searchRcv.adapter = searchResultAdapter
                            searchRcv.layoutManager = LinearLayoutManager(context)
                        } }
                    }
                    Status.ERROR -> {
                        it.message?.let { it1 -> Toast.makeText(context,it1,Toast.LENGTH_LONG) }
                    }
                    Status.LOADING -> {
                    }
                }
            }
        })

}

    companion object {
        @JvmStatic
        fun newInstance() = SearchVideosFragment()
    }
}