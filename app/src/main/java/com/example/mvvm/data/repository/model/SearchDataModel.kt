package com.example.mvvm.data.repository.model

data class SearchDataModel (
    val uri:String?,
    val link:String?,
    val name:String?,
    val description:String?,
    val pictures:Picture?
)

data class Picture(
    val uri:String?,
    val default_picture:Boolean?,
    val sizes:List<Size>?
)


data class Size(
    val link:String?
)