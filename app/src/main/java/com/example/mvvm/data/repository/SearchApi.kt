package com.example.mvvm.data.repository

import com.example.mvvm.BuildConfig
import com.example.mvvm.data.remote.ApiResponse
import com.example.mvvm.data.remote.RetrofitResult
import com.example.mvvm.data.repository.model.SearchDataModel
import com.example.mvvm.data.repository.model.SearchResultListRsp
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    @GET("/videos")
    suspend fun getSearchResult(
        @Query("query") q: String,
        @Query("page") page: Int):SearchResultListRsp
}