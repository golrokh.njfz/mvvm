package com.example.mvvm.data.remote

class ApiResponse<T>(
    val succeeded: Boolean,
    val result: T?,
    val error: ApiResponseError
)

data class ApiResponseError(
    val code: Int,
    val message: String?,
    val details: String?
)

