package com.example.mvvm.data.repository.model

import com.google.gson.annotations.SerializedName

data class SearchResultListRsp (
    val total:Long,
    @SerializedName("data")
    val videos: List<SearchDataModel>?
    )