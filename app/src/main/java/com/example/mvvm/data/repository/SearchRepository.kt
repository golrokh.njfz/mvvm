package com.example.mvvm.data.repository

import com.example.mvvm.core.base.BaseRepository

class SearchRepository(private val searchApi: SearchApi):BaseRepository() {
    suspend fun getSearchResult(q :String,p :Int) = searchApi.getSearchResult(q,p)
}