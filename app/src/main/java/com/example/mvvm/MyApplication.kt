package com.example.mvvm

import android.app.Application
import android.content.Context
import com.example.mvvm.core.di.diModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

open class MyApplication: Application() {
    companion object {
        var mContext: Context = MyApplication.mContext
        private var TAG = MyApplication::class.java.simpleName
    }

    override fun onCreate() {
        super.onCreate()
        mContext = applicationContext
        startKoin {
            androidLogger()
            androidContext(this@MyApplication)
            modules(diModules)
        }
    }
}